package com.example.lebiscuit;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.lebiscuit.utils.Mask;
import com.example.lebiscuit.utils.Utils;
import com.google.android.material.textfield.TextInputEditText;


public class CadastroActivity extends AppCompatActivity {

    private Button buttonvolt;
    private TextView buttonregulamentos;
    private TextInputEditText cpf;
    private TextInputEditText DDD_telefone;
    private TextInputEditText DT_nascimento;
    private Button buttonveficarcpf;

    @Override


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activiy_cadastro);


        //Metudo Para Transferir Tela * Cadastro - Main Activity
        buttonvolt = findViewById(R.id.button_voltar);
        buttonvolt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CadastroActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });//Fim Metuodo

        //Metudo Para Transferir Tela * Cadastro - Regulamentos
        buttonregulamentos = findViewById(R.id.regulamentoscadastro);
        buttonregulamentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CadastroActivity.this, Regulamentos.class);
                startActivity(intent);
            }
        });//Fim Metuodo


        //Metuodo Para Personalização CPF
        cpf = findViewById(R.id.idcpfcadastro);
        cpf.addTextChangedListener(Mask.Companion.mask("###.###.###-##", cpf));
        //Fim Metuodo

        //Metuodo Para Personalização DDD + Celular
        DDD_telefone = findViewById(R.id.DDD_celular);
        DDD_telefone.addTextChangedListener(Mask.Companion.mask("(##)#####-####", DDD_telefone));
        //Fim Metuodo


        //Metuodo Para Personalização DT.NASCIMENTO
        DT_nascimento = findViewById(R.id.data_nascimento);
        DT_nascimento.addTextChangedListener(Mask.Companion.mask("##/##/####", DT_nascimento));
        //Fim Metuodo


        //Metuodo  Para Validação de CPF

        buttonveficarcpf = findViewById(R.id.button_criar_conta);
        buttonveficarcpf.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                String cpfvalid = cpf.getText().toString();

                //CPF INVALDO
                if (!Utils.INSTANCE.isCPFValid(cpfvalid)) {
                    Toast toast = Toast.makeText(CadastroActivity.this, "CPF Invalido", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 300);
                    toast.show();
                }

                //CPF VALIDO
                else if (Utils.INSTANCE.isCPFValid(cpfvalid)) {
                    Toast toast = Toast.makeText(CadastroActivity.this, "CPF Valido", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 300);
                    toast.show();

                }
            }
        });

    }
}


