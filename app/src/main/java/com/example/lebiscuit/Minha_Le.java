package com.example.lebiscuit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.lebiscuit.ui.Home.HomeOptions;

public class Minha_Le extends AppCompatActivity {

    private ImageView buttonVoltarRegulamentos;
    private Button buttonVoltar;
    private Button buttonAceita;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minha__le);


        //Metudo Para Transferir Tela * Minha Le - Regulamentos
        buttonVoltarRegulamentos= findViewById(R.id.button_voltar_regulamentos);
        buttonVoltarRegulamentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Minha_Le.this, Regulamentos.class);
                startActivity(intent);
            }
        });//Fim Metuodo

        //Metudo Para Transferir Tela * Minha Le - Regulamentos

        buttonVoltar= findViewById(R.id.button_Voltar_1);
        buttonVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Minha_Le.this , Regulamentos.class);
                startActivity(intent);
            }
        });

        buttonAceita = findViewById(R.id.button_Aceitar);
        buttonAceita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Minha_Le.this, HomeOptions.class);
                startActivity(intent);
            }
        });




    }
}
