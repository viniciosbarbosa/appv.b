package com.example.lebiscuit.ui.Home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.lebiscuit.CadastroActivity;
import com.example.lebiscuit.R;
import com.google.android.material.navigation.NavigationView;

public class HomeAcitivity extends AppCompatActivity {

    private ImageView Imgface;
    private ImageView Imggmail;
    private ImageView Imglinkedin;
    private Button Buttonvenhacadastra;
    private Button Buttonvenhacadastra2;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ImageView Menu;


    //Metuodo Padrão Da Pagina
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_options);
//       Toolbar toolbar = findViewById(R.id.toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);



        //Metuodo - ImgFacebook
        Imgface = findViewById(R.id.iconeface);
        Imgface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.facebook.com/");

                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
       });//Metudo - ImgFacebook



        //Metuodo - ImgGmail
        Imggmail = findViewById(R.id.iconegmail);
        Imggmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("www.gmail.com");

                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });//Metuodo - ImgGmail


        //Metuodo Linkedin

        Imglinkedin =findViewById(R.id.iconelinkedin);
        Imglinkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.linkedin.com/in/viniciosbarbosaa/");

                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                startActivity(intent);
            }
        });



        Buttonvenhacadastra =findViewById(R.id.button_venhacadastrar);
        Buttonvenhacadastra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeAcitivity.this, CadastroActivity.class);
                startActivity(intent);
            }
        });


        Buttonvenhacadastra2=findViewById(R.id.button_venhacadastrar2);
        Buttonvenhacadastra2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeAcitivity.this,CadastroActivity.class);
                startActivity(intent);
            }
        });

    }// Fim Metuodo Padrão Da Pagina

    private AppBarConfiguration mAppBarConfiguration;



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_options, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


}
