package com.example.lebiscuit.ui.Regulamentos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RegulamentosViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public RegulamentosViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is send fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
