package com.example.lebiscuit.ui.Regulamentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.example.lebiscuit.R;


public class RegulamentosFragment extends Fragment {

    private RegulamentosViewModel regulamentos;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        regulamentos =
                ViewModelProviders.of(this).get(RegulamentosViewModel.class);
        View root = inflater.inflate(R.layout.activity_regulamentos, container, false);

        return root;
    }
}
