package com.example.lebiscuit.ui.Home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.lebiscuit.CadastroActivity;
import com.example.lebiscuit.R;


import static com.example.lebiscuit.R.id.iconeface;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private ImageView Imgface;
    private ImageView Imggmail;
    private ImageView Imglinkedin;
    private Button Buttonvenhacadastra;
    private Button Buttonvenhacadastra2;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
//        homeViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });

        //Metuodo - ImgFacebook
        Imgface = root.findViewById(iconeface);
        Imgface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.facebook.com/");

                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });//Metudo - ImgFacebook



        //Metuodo - ImgGmail
        Imggmail = root.findViewById(R.id.iconegmail);
        Imggmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.gmail.com");

                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });//Metuodo - ImgGmail


        //Metuodo Linkedin

        Imglinkedin =root.findViewById(R.id.iconelinkedin);
        Imglinkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.linkedin.com/in/viniciosbarbosaa/");

                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                startActivity(intent);
            }
        });



        Buttonvenhacadastra =root.findViewById(R.id.button_venhacadastrar);
        Buttonvenhacadastra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CadastroActivity.class);
                startActivity(intent);
            }
        });


        Buttonvenhacadastra2=root.findViewById(R.id.button_venhacadastrar2);
        Buttonvenhacadastra2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CadastroActivity.class);
                startActivity(intent);
            }
        });
        return root;




    }
}