
package com.example.lebiscuit;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.lebiscuit.ui.Home.HomeOptions;
import com.example.lebiscuit.utils.Mask;
import com.example.lebiscuit.utils.Utils;
import com.google.android.material.textfield.TextInputEditText;


public class MainActivity extends AppCompatActivity {


    // Criação do Id Dentro da Classe
    private Button buttoncad;
    private ImageView imagemvoltarmenuprincipal;
    private TextInputEditText idcpf;
    private Button button_entrar;

    @Override


    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Metudo Para Transferir Tela * Main Active - Tela Cadastro
        buttoncad = findViewById(R.id.bt_cadastre);
        buttoncad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CadastroActivity.class);
                startActivity(intent);
            }


        });//Fim Metudo

        //Metudo Para Transferir Tela * Main Active - Home
        imagemvoltarmenuprincipal = findViewById(R.id.buttomvoltarmenuprincipal);
        imagemvoltarmenuprincipal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HomeOptions.class);
                startActivity(intent);
            }
        });//Fim Metudo


        //Metudo Para Personalização CPF
        idcpf = findViewById(R.id.cpfedit);
        idcpf.addTextChangedListener(Mask.Companion.mask("###.###.###-##", idcpf));
        //Fim Metudo



        //Metudo Para Validação de CPF
        button_entrar = findViewById(R.id.buttonentrar);
        button_entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cpfmenu = idcpf.getText().toString();

                //CPF INVALDO
                if (!Utils.INSTANCE.isCPFValid(cpfmenu)) {
                    Toast toast = Toast.makeText(MainActivity.this, "CPF Invalido", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 300);
                    toast.show();


                }

                //CPF VALIDO
                else if (Utils.INSTANCE.isCPFValid(cpfmenu)) {
                    Toast toast = Toast.makeText(MainActivity.this, "CPF Valido", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 300);
                    toast.show();
                }


            }
        });//Fim Metuodo

    }


}